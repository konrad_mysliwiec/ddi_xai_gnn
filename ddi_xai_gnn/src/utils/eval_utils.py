import torch
from typing import Tuple

from ddi_xai_gnn.src.data.id_mappers import create_node_id_to_name_mapper, \
    create_node_id_to_orig_id_mapper


class DatasetNodeMapper(object):
    def __init__(self, dataset_dir: str) -> None:
        super().__init__()
        self.dataset_dir = dataset_dir

        self.node_id_to_name_mapper = \
            create_node_id_to_name_mapper(dataset_dir)

        self.node_id_to_orig_id_mapper = \
            create_node_id_to_orig_id_mapper(dataset_dir)

    def get_node_names_from_edge(
        self,
        tensor_edge_id: int,
        edge_index: torch.Tensor
    ) -> Tuple[str, str]:
        """Maps encoded node ids to their names for a given edge.

        Args:
            tensor_edge_id (int): Edge id to retrive node names for.
            edge_index (torch.Tensor): Edge index.

        Returns:
            Tuple[str, str]: Tuple with disease and drug name.
        """
        edge = edge_index[:, tensor_edge_id]
        if edge[0] < edge[1]:
            drug = self.node_id_to_name_mapper.get(edge[0].item())
            disease = self.node_id_to_name_mapper.get(edge[1].item())
        else:
            disease = self.node_id_to_name_mapper.get(edge[0].item())
            drug = self.node_id_to_name_mapper.get(edge[1].item())
        return disease, drug

    def get_node_orig_ids_from_edge(
        self,
        tensor_edge_id: int,
        edge_index: torch.Tensor
    ) -> Tuple[str, str]:
        """Maps encoded node ids to their original ids for a given edge.

        Args:
            tensor_edge_id (int): Edge id to retrive node names for.
            edge_index (torch.Tensor): Edge index.

        Returns:
            Tuple[str, str]: Tuple with disease and drug original node ids.
        """
        edge = edge_index[:, tensor_edge_id]
        if edge[0] < edge[1]:
            drug = self.node_id_to_orig_id_mapper.get(edge[0].item())
            disease = self.node_id_to_orig_id_mapper.get(edge[1].item())
        else:
            disease = self.node_id_to_orig_id_mapper.get(edge[0].item())
            drug = self.node_id_to_orig_id_mapper.get(edge[1].item())
        return disease, drug
