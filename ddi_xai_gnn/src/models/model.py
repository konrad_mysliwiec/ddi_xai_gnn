import torch
import torch.nn.functional as F
import torch.nn as nn
import torch_geometric.nn as pyg_nn
from ddi_xai_gnn.src.utils.args import Args


class GNNStack(torch.nn.Module):
    def __init__(
        self,
        input_dim: int,
        hidden_dim: int,
        output_dim: int,
        args: Args
    ):
        super(GNNStack, self).__init__()
        conv_model = self.get_model(args.model_name)
        self.convs_layers = nn.ModuleList()
        self.convs_layers.append(
            conv_model(input_dim, hidden_dim, aggr=args.aggr,
            add_self_loops=args.add_self_loops)
        )
        self.norm_layers = nn.ModuleList()
        self.norm_layers.append(nn.LayerNorm(hidden_dim))

        if output_dim is None:
            output_dim = hidden_dim

        for _ in range(args.num_layers-1):
            self.convs_layers.append(
                conv_model(
                    hidden_dim,
                    hidden_dim,
                    aggr=args.aggr,
                    add_self_loops=args.add_self_loops
                )
            )
            self.norm_layers.append(nn.LayerNorm(hidden_dim))

        self.final_lin_layers = nn.Sequential(
            nn.Linear(hidden_dim, hidden_dim),
            nn.Dropout(args.dropout),
            nn.Linear(hidden_dim, output_dim)
        )
        self.task = args.task
        self.dropout = args.dropout
        self.num_layers = args.num_layers

    def get_model(self, model_name):
        if model_name == 'GCN':
            return pyg_nn.GCNConv
        elif model_name == 'GraphSage':
            return pyg_nn.SAGEConv
        elif model_name == 'GAT':
            return pyg_nn.GATConv
        else:
            raise RuntimeError("Unknown GNN model.")

    def forward(self, data, edge_index=None, edge_weight=None):
        x = data.x

        if edge_index is None:
            edge_index = data.edge_index

        for layer in range(self.num_layers):
            x = self.convs_layers[layer](
                x, edge_index, edge_weight=edge_weight
            )
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout, training=self.training)

        if self.task == 'node':
            x = self.final_lin_layers(x)
            out = F.log_softmax(x, dim=1)
        elif self.task == 'link':
            out = x
        else:
            raise RuntimeError('Unknown task.')

        return out

    def loss(self, pred, label, weights=None):
        if self.task == 'node':
            return F.nll_loss(pred, label, weight=weights)
        else:
            pass
