class Args:
    opt = 'adam'
    opt_scheduler = 'none'
    opt_restart = None
    opt_decay_step = None
    opt_decay_rate = None
    lr = 0.01
    clip = None
    weight_decay = 0.0
    model_name = 'GCN'
    batch_size = 32
    num_layers = 2
    hidden_dim = 32
    dropout = 0.0
    epochs = 200
    task = 'node'
    aggr = 'mean'
    add_self_loops = False
