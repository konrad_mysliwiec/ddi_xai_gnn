import torch
import torch.nn as nn
import torch_geometric.nn as pyg_nn
from torch_geometric.data import DataLoader, Dataset
from ddi_xai_gnn.src.models.model import GNNStack
from sklearn.metrics import average_precision_score, \
    balanced_accuracy_score, \
    roc_auc_score
from ddi_xai_gnn.src.utils.utils import create_directory
from ddi_xai_gnn.src.utils.args import Args


def build_optimizer(args: Args, params):
    weight_decay = args.weight_decay
    filter_fn = filter(lambda p: p.requires_grad, params)
    if args.opt == 'adam':
        optimizer = torch.optim.Adam(
            filter_fn,
            lr=args.lr,
            weight_decay=weight_decay
        )
    elif args.opt == 'sgd':
        optimizer = torch.optim.SGD(
            filter_fn,
            lr=args.lr,
            momentum=0.95,
            weight_decay=weight_decay
        )
    elif args.opt == 'rmsprop':
        optimizer = torch.optim.RMSprop(
            filter_fn,
            lr=args.lr,
            weight_decay=weight_decay
        )
    elif args.opt == 'adagrad':
        optimizer = torch.optim.SGD(
            filter_fn,
            lr=args.lr,
            weight_decay=weight_decay
        )
    if args.opt_scheduler == 'none':
        scheduler = torch.optim.lr_scheduler.StepLR(
            optimizer,
            step_size=args.opt_decay_step,
            gamma=args.opt_decay_rate
        )
    elif args.opt_scheduler == 'cos':
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
            optimizer,
            T_max=args.opt_restart
        )
    return scheduler, optimizer


def calculate_weights(labels: torch.tensor):
    if labels.shape[0] == 0:
        return None
    labels_ratio = torch.bincount(labels)/labels.shape[0]
    reversed_ratio = torch.tensor([1])/labels_ratio
    return reversed_ratio / reversed_ratio.sum()


def train(
    dataset: Dataset,
    args: Args,
    learning_type: str = 'subgraphs',
    writer=None,
    checkpoint_dir: str = None
):
    task = args.task
    test_loader = train_loader = DataLoader(
        dataset,
        batch_size=args.batch_size,
        shuffle=False
    )
    if task == 'link':
        model = pyg_nn.GAE(
            GNNStack(
                dataset.num_node_features,
                args.hidden_dim,
                None,
                args
            )
        )
    elif task == 'node':
        model = GNNStack(
                dataset.num_node_features,
                args.hidden_dim,
                int(dataset.num_classes),
                args
        )
    else:
        RuntimeError('Task not implemented.')

    scheduler, opt = build_optimizer(args, model.parameters())

    for epoch in range(args.epochs):
        total_loss = 0
        model.train()
#         train_precisions = []
        for batch in train_loader:
            opt.zero_grad()
            if task == 'node':
                pred = model(batch)
                label = batch.y
                pred = pred[batch.train_mask]
                label = label[batch.train_mask]
                weights = calculate_weights(label)
                loss = model.loss(pred, label, weights)
            elif task == 'link':
                train_pos_edge_index = batch.train_pos_edge_index
                train_neg_edge_index = batch.train_neg_edge_index
                z = model.encode(batch, train_pos_edge_index)
                loss = model.recon_loss(
                    z,
                    train_pos_edge_index,
                    train_neg_edge_index
                )
#                 _, train_precision = model.test(
#                     z,
#                     train_pos_edge_index,
#                     train_neg_edge_index
#                 )
#                 train_precisions.append(train_precision)
            loss.backward()
            opt.step()
            total_loss += loss.item() * batch.num_graphs  # Check num_graphs
        total_loss /= len(train_loader.dataset)
        if writer:
            writer.add_scalar("loss", total_loss, epoch)
        print(total_loss)

        if epoch % 1 == 0:
            if task == 'link':
                pass
                avg_precision_score, roc_auc_score = test(
                    test_loader,
                    model,
                    task,
                    learning_type
                )
                if writer:
                    writer.add_scalar(
                        "Test avg precision",
                        avg_precision_score,
                        epoch
                    )
                    writer.add_scalar(
                        "Test roc auc score",
                        roc_auc_score,
                        epoch
                    )
                print("Test avg precision:", avg_precision_score)
                print("Test roc auc score", roc_auc_score)
            elif task == 'node':
                test_metric = test(
                    test_loader,
                    model,
                    task=task,
                    learning_type=learning_type
                )
                if writer:
                    writer.add_scalar("Test metric", test_metric, epoch)
                print("Test metric:", test_metric)
            if checkpoint_dir:
                create_directory(checkpoint_dir)
                checkpoint = {
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': opt.state_dict(),
                    'epoch': epoch
                }
                torch.save(
                    checkpoint,
                    f'{checkpoint_dir}/checkpoint_{epoch}.pth'
                )


def test(
    loader: DataLoader,
    model: nn.Module,
    task: str,
    learning_type: str = 'subgraphs',
    is_validation: bool = False
):
    model.eval()
    if task == 'node':
        preds = []
        labels = []
        for data in loader:
            with torch.no_grad():
                pred = model(data).max(dim=1)[1]
            preds.append(pred)
            labels.append(data.y)
        preds = torch.cat(preds, dim=0)
        labels = torch.cat(labels, dim=0)
        return balanced_accuracy_score(labels, pred)
    elif task == 'link':
        test_prec = []
        y = []
        pred = []
        pos_preds = []
        neg_preds = []
        for data in loader:
            with torch.no_grad():
                if learning_type == 'subgraphs':
                    pos_edge_index = data.test_pos_edge_index
                elif learning_type == 'transductive':
                    pos_edge_index = torch.cat(
                        [
                            data.train_pos_edge_index,
                            data.test_pos_edge_index
                        ]
                    )
                z = model.encode(data, pos_edge_index)
                test_prec.append(
                    model.test(
                        z,
                        data.test_pos_edge_index,
                        data.test_neg_edge_index
                    )[1].item()
                )
                pos_y = z.new_ones(data.test_pos_edge_index.size(1))
                neg_y = z.new_zeros(data.test_neg_edge_index.size(1))
                y.append(pos_y)
                y.append(neg_y)
                pos_pred = model.decoder(
                    z,
                    data.test_pos_edge_index,
                    sigmoid=True
                )
                neg_pred = model.decoder(
                    z,
                    data.test_neg_edge_index,
                    sigmoid=True
                )
                pred.append(pos_pred)
                pred.append(neg_pred)
                pos_preds.append(pos_pred)
                neg_preds.append(neg_pred)
        y = torch.cat(y, dim=0)
        pred = torch.cat(pred, dim=0)
        return average_precision_score(y, pred), roc_auc_score(y, pred)
