import torch
import torch.nn.functional as F
import torch.nn as nn
import torch_geometric.nn as pyg_nn
from ddi_xai_gnn.src.utils.args import Args


class RGCNEncoder(torch.nn.Module):
    def __init__(
        self,
        input_channels,
        hidden_channels,
        num_relations,
        args: Args,
        output_dim: int = None
    ):
        super().__init__()
        if output_dim is None:
            output_dim = hidden_channels

        self.convs_layers = nn.ModuleList()
        self.convs_layers.append(
            pyg_nn.RGCNConv(
                input_channels,
                hidden_channels,
                num_relations,
                aggr=args.aggr
                # num_blocks=5
                )
            )
        self.norm_layers = nn.ModuleList()
        self.norm_layers.append(nn.LayerNorm(hidden_channels))

        for _ in range(args.num_layers-1):
            self.convs_layers.append(
                pyg_nn.RGCNConv(
                    hidden_channels,
                    hidden_channels,
                    num_relations,
                    aggr=args.aggr
                    # num_blocks=5
                )
            )
            self.norm_layers.append(nn.LayerNorm(hidden_channels))

        self.final_lin_layers = nn.Sequential(
            nn.Linear(hidden_channels, hidden_channels),
            nn.Dropout(args.dropout),
            nn.Linear(hidden_channels, output_dim)
        )
        self.task = args.task
        self.dropout = args.dropout
        self.num_layers = args.num_layers
        self.reset_parameters()

    def reset_parameters(self):
        for layer in range(self.num_layers):
            self.convs_layers[layer].reset_parameters()

    def forward(self, data, edge_index, edge_type):
        x = data.x

        for layer in range(self.num_layers):
            x = self.convs_layers[layer](x, edge_index, edge_type)
            # x = self.norm_layers[layer](x)
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout, training=self.training)

        if self.task == 'node':
            x = self.final_lin_layers(x)
            out = F.log_softmax(x, dim=1)
        elif self.task == 'link':
            out = x
        else:
            raise RuntimeError('Unknown task.')

        return out

    def loss(self, pred, label, weights=None):
        if self.task == 'node':
            return F.nll_loss(pred, label, weight=weights)
        else:
            pass


class DistMultDecoder(torch.nn.Module):
    def __init__(self, num_relations, hidden_channels):
        super().__init__()
        self.rel_emb = nn.Parameter(
            torch.Tensor(num_relations, hidden_channels)
        )
        self.reset_parameters()

    def reset_parameters(self):
        torch.nn.init.xavier_uniform_(self.rel_emb)

    def forward(self, z, edge_index, edge_type):
        z_src, z_dst = z[edge_index[0]], z[edge_index[1]]
        rel = self.rel_emb[edge_type]
        return torch.sum(z_src * rel * z_dst, dim=1)
