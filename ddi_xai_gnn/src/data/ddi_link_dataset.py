from ddi_xai_gnn.src.data.ddi_preprocessing import \
    download_miner_disease_chemical, preprocess_miner_disease_chemical
from ddi_xai_gnn.src.utils.utils import create_directory, load_from_pickle
import os.path as osp
import pandas as pd
from typing import Callable, List, Optional, Tuple, Union
from torch_geometric.data import Data, InMemoryDataset
from torch_geometric.utils import negative_sampling, to_undirected
from ddi_xai_gnn.src.utils.neg_sampling import relational_negative_sampling
import torch_geometric as pyg
import torch
import math
import ddi_xai_gnn.src.utils.settings as settings


def get_ddi_links_data(
    egde_csv_file_path: str,
    nodes_labels_file_path: str,
    root_dir: str,
    features: list = None,
    labels_dir: list = None,
    val_ratio=0.2,
    test_ratio=0.1
) -> pyg.data.Data:
    """Returns Data object for DDI network for node classification task.

    Args:
        egde_csv_file_path (str): Filepath to a csv file with edge index.
        nodes_labels_file_path (str): Filepath to a file with nodes labels.
        root_dir (str): Root directory for dataset.
        features (str, optional): Nodes features. Defaults to 'ones'.
        labels_dir (str, optional): Filepath to a file with
            nodes' classes labels. If provided the negative samples for
            relations will be drawn only between these classed of nodes which
            are related in the positive samples.
        val_ratio (float, optional): Validation ratio for a dataset.
            Defaults to 0.2.
        test_ratio (float, optional): Test ratio for a dataset.
            Defaults to 0.1.

    Returns:
        pyg.data.Data: Data for DDI network.
    """
    edge_index_df = pd.read_csv(egde_csv_file_path)
    nodes_labels = pd.read_csv(nodes_labels_file_path)
    edge_index = torch.tensor(
        [
            edge_index_df['source'],
            edge_index_df['target']
        ],
        dtype=torch.long
    )
    edge_index = to_undirected(edge_index)
    nodes_labels = torch.tensor(nodes_labels['label'], dtype=torch.long)
    num_nodes = edge_index.max()+1
    if features is None:
        x = torch.ones(num_nodes, 10, dtype=torch.float32)
    else:
        x = torch.tensor(features, dtype=torch.float32)

    data = Data(
        x=x,
        edge_index=edge_index,
        num_nodes=num_nodes,
        nodes_labels=nodes_labels
    )
    if labels_dir:
        labels = pd.read_csv(labels_dir)
        valid_nodes_connections = [
            [
                torch.tensor(labels['node'][labels['label'] == 0].to_list()),
                torch.tensor(labels['node'][labels['label'] == 1].to_list())
            ]
        ]
    else:
        valid_nodes_connections = None
    data = custom_train_test_split_edges(
        data,
        val_ratio,
        test_ratio,
        valid_nodes_connections
    )
    encoder_dir = osp.join(
        root_dir,
        "processed",
        "label_encoder.pkl"
    )
    labels_dict_dir = osp.join(
        root_dir,
        "processed",
        "labels_dict.pkl"
    )
    data = _get_data_edges_type(
        data,
        encoder_dir,
        labels_dict_dir
        )

    return data


def custom_train_test_split_edges(
    data: Data,
    val_ratio: float = 0.05,
    test_ratio: float = 0.1,
    valid_nodes_connections: list = None
) -> Data:
    """Splits data into train-test-validations sets and adds negative smapling.

    Args:
        data (pyg.data.Data): Data to be split.
        val_ratio (float, optional): Validation ratio. Defaults to 0.05.
        test_ratio (float, optional): Test ratio. Defaults to 0.1.
        valid_nodes_connections (list, optional):
            List of two element lists of tensors with nodes of
            two nodes classes' which are allowed to be related to each other.
            For example tensor with nodes with drug nodes and tensor with nodes
            for disease nodes - provides nodes of two types which can be
            related.
            Defaults to None.

    Returns:
        pyg.data.Data: Data with positive and negative samples
            split into train-test-validation sets. The splits are accessible
            in the Data object under the following attributes:
            - train_pos_edge_type
            - test_pos_edge_type
            - val_pos_edge_type
            - train_neg_edge_type
            - test_neg_edge_type
            - val_neg_edge_type
            
    """
    num_nodes = data.num_nodes
    row, col = data.edge_index
    edge_index = data.edge_index
    data.edge_index = None
    # mask = row < col
    # row, col = row[mask], col[mask]
    n_v = int(math.floor(val_ratio * row.size(0)))
    n_t = int(math.floor(test_ratio * row.size(0)))

    # Positive edges
    perm = torch.randperm(row.size(0))
    row, col = row[perm], col[perm]

    r, c = row[:n_v], col[:n_v]
    data.val_pos_edge_index = torch.stack([r, c], dim=0)

    r, c = row[n_v: n_v+n_t], col[n_v:n_v+n_t]
    data.test_pos_edge_index = torch.stack([r, c], dim=0)

    r, c = row[n_v+n_t:], col[n_v+n_t:]
    data.train_pos_edge_index = torch.stack([r, c], dim=0)

    # Negative edges
    num_samples = (
        data.val_pos_edge_index.shape[1] +
        data.test_pos_edge_index.shape[1] +
        data.train_pos_edge_index.shape[1]
    )
    if valid_nodes_connections is None:
        neg_adj_mask = negative_sampling(
            edge_index,
            num_nodes=num_nodes,
            num_neg_samples=num_samples
        )
    else:
        neg_adj_mask = relational_negative_sampling(
            edge_index,
            num_nodes=num_nodes,
            num_neg_samples=num_samples,
            valid_nodes_connections=valid_nodes_connections
        )
    neg_row, neg_col = neg_adj_mask
    perm = torch.randperm(neg_row.size(0))
    neg_row, neg_col = neg_row[perm], neg_col[perm]

    n_r, n_c = neg_row[:n_v], neg_col[:n_v]
    data.val_neg_edge_index = torch.stack([n_r, n_c], dim=0)

    n_r, n_c = neg_row[n_v: n_v+n_t], neg_col[n_v: n_v+n_t]
    data.test_neg_edge_index = torch.stack([n_r, n_c], dim=0)

    n_r, n_c = neg_row[n_v+n_t:], neg_col[n_v+n_t:]
    data.train_neg_edge_index = torch.stack([n_r, n_c], dim=0)

    return data


def _get_data_edges_type(
    data: pyg.data.Data,
    encoder_dir: str,
    labels_dict_dir: str
) -> pyg.data.Data:
    """Adds attributes for edges types in a provided Data.

    Args:
        data (pyg.data.Data): Data with network which
            edges types should be provided for.
        encoder_dir (str): Directory to the label encoder with original ids to
            encoded id mappings.
        labels_dict_dir (str): Directory to the dictionary with original ids to
            nodes' classes mappings.

    Returns:
        pyg.data.Data: Input PyG Data object with additional with
            edges types information. The edges types are accessible from
            the following Data object attributes:
            - train_pos_edge_type
            - test_pos_edge_type
            - val_pos_edge_type
            - train_neg_edge_type
            - test_neg_edge_type
            - val_neg_edge_type
    """
    data.test_neg_edge_type = _get_edges_type(
        data.test_neg_edge_index,
        encoder_dir,
        labels_dict_dir
    )
    data.train_neg_edge_type = _get_edges_type(
        data.train_neg_edge_index,
        encoder_dir,
        labels_dict_dir
    )
    data.val_neg_edge_type = _get_edges_type(
        data.val_neg_edge_index,
        encoder_dir,
        labels_dict_dir
    )
    data.test_pos_edge_type = _get_edges_type(
        data.test_pos_edge_index,
        encoder_dir,
        labels_dict_dir
    )
    data.train_pos_edge_type = _get_edges_type(
        data.train_pos_edge_index,
        encoder_dir,
        labels_dict_dir
    )
    data.val_pos_edge_type = _get_edges_type(
        data.val_pos_edge_index,
        encoder_dir,
        labels_dict_dir
    )

    return data


def _get_edges_type(
    edge_index: torch.Tensor,
    encoder_dir: str,
    labels_dict_dir: str
) -> torch.Tensor:
    """Returnes edges type for nodes in a given edge_index.

    Args:
        edge_index (torch.Tensor): Edge index which
            edges types should be provided for.
        encoder_dir (str): Directory to the label encoder with original ids to
            encoded id mappings.
        labels_dict_dir (str): Directory to the dictionary with original ids to
            nodes' classes mappings.

    Returns:
        torch.Tensor: 1D tensor with edge types.
    """
    label_encoder = load_from_pickle(encoder_dir)
    labels_dict = load_from_pickle(labels_dict_dir)
    edge_index_source = edge_index[0].tolist()
    labels = [
        labels_dict[orig_id]
        for orig_id
        in label_encoder.inverse_transform(edge_index_source)
    ]
    labels = torch.Tensor(labels)
    return labels


class DiseaseInteractionNetworkLinkDataset(InMemoryDataset):
    def __init__(
        self,
        root: Optional[str],
        transform: Optional[Callable] = None,
        pre_transform: Optional[Callable] = None,
        pre_filter: Optional[Callable] = None,
        test_ratio: float = 0.1,
        val_ratio: float = 0.2,
        features: Optional[List] = None
    ):
        self.features = features
        self.val_ratio = val_ratio
        self.test_ratio = test_ratio
        super(DiseaseInteractionNetworkLinkDataset, self).__init__(
            root=root,
            transform=transform,
            pre_transform=pre_transform,
            pre_filter=pre_filter,
        )
        self.data, self.slices = torch.load(self.processed_paths[0])
        create_directory(root)

    @property
    def raw_file_names(self) -> Union[str, List[str], Tuple]:
        return []

    @property
    def processed_dir(self) -> str:
        return osp.join(self.root, 'processed')

    @property
    def processed_file_names(self) -> Union[str, List[str], Tuple]:
        return ['data_0.pt']

    def download(self):
        download_miner_disease_chemical(self.root)

    def process(self):
        edge_index_dir, nodes_labels_dir = \
            preprocess_miner_disease_chemical(self.root)
        labels_dir = osp.join(
                self.root,
                settings.DDI_NODES_LABELS
            )
        data_list = [
            get_ddi_links_data(
                edge_index_dir,
                nodes_labels_dir,
                self.root,
                self.features,
                labels_dir,
                self.val_ratio,
                self.test_ratio
            )
        ]
        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])
