import pandas as pd
import os.path as osp
import ddi_xai_gnn.src.utils.settings as settings
from ddi_xai_gnn.src.utils.utils import load_from_pickle


def create_id_to_name_mapper(dataset_dir: str):
    edge_index_path = osp.join(
        dataset_dir,
        'raw_data',
        settings.EDGE_INDEX_FILE_NAME
    )
    ddi_df = pd.read_csv(edge_index_path, sep='\t')
    drug_ids = ddi_df['Chemical'].unique()
    disease_ids = ddi_df['# Disease(MESH)'].unique()
    drug_data_path = osp.join(
        dataset_dir,
        'raw_data',
        settings.DRUG_DATA_FILE_NAME
    )
    drug_df = pd.read_csv(drug_data_path)
    drug_df = drug_df[drug_df['DrugBank ID'].isin(drug_ids)]
    disease_data_path = osp.join(
        dataset_dir,
        'raw_data',
        settings.DISEASE_DATA_FILE_NAME
    )
    disease_df = pd.read_csv(disease_data_path, sep='\t')
    disease_df = disease_df[disease_df['# MESH_ID'].isin(disease_ids)]
    drug_mapper_df = drug_df[['DrugBank ID', 'Common name']].rename(
        {
            "DrugBank ID": "id",
            "Common name": "name"
        },
        axis=1
    )
    disease_mapper_df = disease_df[['# MESH_ID', 'Name']].rename(
        {
            "# MESH_ID": "id",
            "Name": "name"
        },
        axis=1
    )
    mapper_df = pd.concat([drug_mapper_df, disease_mapper_df])
    mapper = {
        record['id']: record['name']
        for record in mapper_df.to_dict(orient='records')
    }
    return mapper


def create_node_id_to_name_mapper(dataset_dir: str):
    id_to_name_mapper = create_id_to_name_mapper(dataset_dir)
    encoder_dir = osp.join(
        dataset_dir,
        'processed',
        'label_encoder.pkl'
    )
    le = load_from_pickle(encoder_dir)
    mapper = dict(
        zip(
            le.transform(list(id_to_name_mapper.keys())),
            list(id_to_name_mapper.values())
        )
    )
    return mapper


def create_node_id_to_orig_id_mapper(dataset_dir: str):
    node_id_to_name_mapper = create_node_id_to_name_mapper(dataset_dir)
    name_to_id_mapper = {
        name: orig_id
        for orig_id, name in create_id_to_name_mapper(dataset_dir).items()
    }
    mapper = {
        node_id: name_to_id_mapper[name]
        for node_id, name in node_id_to_name_mapper.items()
    }
    return mapper


def create_node_id_to_label_mapper(
    dataset_dir: str
) -> dict:
    labels_dict_dir = osp.join(
        dataset_dir,
        'processed',
        'labels_dict.pkl'
    )
    labels_dict = load_from_pickle(labels_dict_dir)
    orig_id_to_id_mapper = {
        orig_id: node_id
        for node_id, orig_id
        in create_node_id_to_orig_id_mapper(dataset_dir).items()
    }
    mapper = {
        orig_id_to_id_mapper[orig_id]: label
        for orig_id, label in labels_dict.items()
    }
    return mapper
