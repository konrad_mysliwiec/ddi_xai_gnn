from ddi_xai_gnn.src.data.ddi_preprocessing import download_miner_disease_chemical,\
    preprocess_miner_disease_chemical
from ddi_xai_gnn.src.utils.utils import create_directory
import os.path as osp
import numpy as np
import pandas as pd
import random
from typing import Callable, List, Optional, Tuple, Union
from torch_geometric.data import Data, InMemoryDataset
from torch_geometric.utils import to_undirected
import torch_geometric
import torch


def get_ddi_nodes_data(
    egde_csv_file_path: str,
    nodes_labels_file_path: str,
    features: list = None,
    val_ratio=0.2,
    test_ratio=0.1
) -> torch_geometric.data.Dataset:
    """Returns Data object for DDI network for node classification task.

    Args:
        egde_index_file_path (str): Filepath to a csv file with edge index.
        nodes_labels_file_path (str): Filepath to a file with nodes labels.
        features (str, optional): Nodes features. Defaults to 'ones'.
        val_ratio (float, optional): Validation ratio for a dataset.
            Defaults to 0.2.
        test_ratio (float, optional): Test ratio for a dataset.
            Defaults to 0.1.

    Returns:
        torch_geometric.data.Dataset: Dataset for DDI network.
    """
    edge_index_df = pd.read_csv(egde_csv_file_path)
    nodes_labels = pd.read_csv(nodes_labels_file_path)
    edge_index = torch.tensor(
        [
            edge_index_df['source'],
            edge_index_df['target']
        ],
        dtype=torch.long
    )
    edge_index = to_undirected(edge_index)
    y = torch.tensor(nodes_labels['label'], dtype=torch.long)
    num_nodes = len(y)
    if features is None:
        x = torch.ones(num_nodes, 10, dtype=torch.float32)
    else:
        x = torch.tensor(features, dtype=torch.float32)
    dataset_masks = creates_masks(
        nodes_labels['node'],
        val_ratio,
        test_ratio
    )
    num_classes = torch.unique(y).size(0)
    data = Data(
        y=y,
        x=x,
        edge_index=edge_index,
        num_nodes=num_nodes,
        num_classes=num_classes,
        test_mask=dataset_masks['test'],
        train_mask=dataset_masks['train'],
        val_mask=dataset_masks['validation']
    )
    return data


def creates_masks(nodes_list: List, val_ratio: float, test_ratio: float):
    """Creates a random mask for train, validation and test data.

    Args:
        nodes_list (List): List of nodes to create masks from.
        val_ratio (float): Validation ratio.
        test_ratio (float): Test ratio.

    Returns:
        [Dict[str, torch.tensor]]: A dictionary with split data.
            The keys are: 'train', 'validation', 'test' and the values are
            tensors with boolean values which indicates if a given node is
            part of a specific dataset.
    """
    num_nodes = len(nodes_list)
    val_len = int(num_nodes * val_ratio)
    test_len = int(num_nodes * test_ratio)
    train_len = num_nodes - val_len - test_len
    len_dictionary = {
        "validation": val_len,
        "test": test_len,
        "train": train_len
    }
    datasets = {}
    labels_set = set(nodes_list)

    for dataset_type in len_dictionary.keys():
        temp_list = list(labels_set)
        sampling = random.sample(temp_list, k=len_dictionary[dataset_type])
        datasets[dataset_type] = torch.tensor(
            np.isin(nodes_list, sampling),
            dtype=torch.bool
        )
        labels_set.difference_update(set(sampling))

    return datasets


class DiseaseInteractionNetworkNodeDataset(InMemoryDataset):
    def __init__(
        self,
        root: Optional[str],
        transform: Optional[Callable] = None,
        pre_transform: Optional[Callable] = None,
        pre_filter: Optional[Callable] = None,
        test_ratio: float = 0.1,
        val_ratio: float = 0.2,
        features: Optional[List] = None
    ):
        self.features = features
        self.val_ratio = val_ratio
        self.test_ratio = test_ratio
        super(DiseaseInteractionNetworkNodeDataset, self).__init__(
            root=root,
            transform=transform,
            pre_transform=pre_transform,
            pre_filter=pre_filter,
        )
        self.data, self.slices = torch.load(self.processed_paths[0])
        create_directory(root)

    @property
    def raw_file_names(self) -> Union[str, List[str], Tuple]:
        return []

    @property
    def processed_dir(self) -> str:
        return osp.join(self.root, 'processed')

    @property
    def processed_file_names(self) -> Union[str, List[str], Tuple]:
        return ['data_0.pt']

    def download(self):
        download_miner_disease_chemical(self.root)

    def process(self):
        edge_index_dir, nodes_labels_dir = \
            preprocess_miner_disease_chemical(self.root)
        data_list = [
            get_ddi_nodes_data(
                edge_index_dir,
                nodes_labels_dir,
                self.features,
                self.val_ratio,
                self.test_ratio
            )
        ]
        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])
