import os.path as osp

# Drug disease interaction network
DISEASE_DATA_FILE_NAME = 'D-MeshMiner_miner-disease.tsv'
DISEASE_DATA_PATH = osp.join(
    'ddi_xai_gnn',
    'data',
    'raw_data',
    DISEASE_DATA_FILE_NAME
)
DRUG_DATA_FILE_NAME = 'drugbank vocabulary.csv'
DRUG_DATA_PATH = osp.join(
    'ddi_xai_gnn',
    'data',
    'raw_data',
    DRUG_DATA_FILE_NAME
)
EDGE_INDEX_FILE_NAME = 'DCh-Miner_miner-disease-chemical.tsv'
DDI_EDGE_INDEX_PATH = osp.join(
    'ddi_xai_gnn',
    'data',
    'raw_data',
    'DCh-Miner_miner-disease-chemical.tsv'
)
DDI_EDGE_INDEX_ENCODED_PATH = osp.join(
    'ddi_xai_gnn',
    'data',
    'processed',
    'disease_chemical_edge_index.csv'
)

# Pytrch Geo datasets
DDI_NO_FEATURES_PATH = 'disease_interaction_data_no_features'
DDI_STRUC2VEC_FEATURES_PATH = 'disease_interaction_data_struc2vec_full'

DDI_NODES_LABELS = osp.join(
    "processed",
    "disease_chemical_nodes_labels.csv"
)
