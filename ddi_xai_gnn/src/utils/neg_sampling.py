import torch
import random


def relational_negative_sampling(
    edge_index: torch.Tensor,
    num_nodes: int,
    num_neg_samples: int,
    valid_nodes_connections: list,
    force_undirected: bool = True
) -> torch.Tensor:
    valid_nodes_connections = [
        torch.cartesian_prod(nodes_type_a, nodes_type_b)
        for nodes_type_a, nodes_type_b in valid_nodes_connections
    ]
    if force_undirected:
        valid_nodes_connections = [
            torch.cat(
                [
                    sub_edge_index,
                    reverse_edges(sub_edge_index)
                ]
            )
            for sub_edge_index in valid_nodes_connections
        ]
    if num_neg_samples is None:
        num_neg_samples = num_nodes
    allowed_A = torch.zeros(num_nodes, num_nodes)
    for valid_nodes in valid_nodes_connections:
        allowed_A[
            [
                tuple(valid_nodes.T[0].tolist()),
                tuple(valid_nodes.T[1].tolist())
            ]
        ] = 1
    allowed_A[
        [
            tuple(edge_index[0].tolist()),
            tuple(edge_index[1].tolist())
        ]
    ] = 0
    neg_edges = allowed_A.nonzero()
    perm = torch.tensor(
        random.sample(
            range(neg_edges.shape[0]),
            num_neg_samples
        )
    )
    neg_edges = neg_edges[perm]
    return neg_edges.T


def reverse_edges(edge_index):
    if edge_index.shape[1] == 2:
        return torch.tensor(
            [
                edge_index[:, 1].tolist(),
                edge_index[:, 0].tolist()
            ]
        ).T
    elif edge_index.shape[0] == 2:
        return torch.tensor(
            [
                edge_index[1].tolist(),
                edge_index[0].tolist()
            ]
        )
