from torch_geometric.data.download import download_url
import gzip
import os.path as osp
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from typing import Tuple
import requests
import zipfile
from ddi_xai_gnn.src.utils.utils import create_directory, save_to_pickle

URL = 'http://snap.stanford.edu/biodata/datasets/10004/files/'\
    'DCh-Miner_miner-disease-chemical.tsv.gz'
FILE_NAME = 'DCh-Miner_miner-disease-chemical.tsv'

DISEASE_URL = 'https://snap.stanford.edu/biodata/datasets/10003/files/'\
    'D-MeshMiner_miner-disease.tsv.gz'
DISEASE_FILE_NAME = 'D-MeshMiner_miner-disease.tsv'

DRUG_URL = 'https://go.drugbank.com/releases/5-1-8/downloads/'\
    'all-drugbank-vocabulary'

DRUG_FILE_NAME = 'drugbank vocabulary.csv'


def download_miner_disease_chemical(directory: str):
    """Downloads Drug Disease Interaction dataset from SNAP with nodes mapping.

    Args:
        directory (str): Root directory for storing the data.
    """
    _download_and_unzip(
        file_url=URL,
        dataset_dir=directory,
        unzipped_file_name=FILE_NAME,
        zip_type='gz'
    )
    _download_and_unzip(
        file_url=DISEASE_URL,
        dataset_dir=directory,
        unzipped_file_name=DISEASE_FILE_NAME,
        zip_type='gz'
    )
    _download_and_unzip(
        file_url=DRUG_URL,
        dataset_dir=directory,
        unzipped_file_name=DRUG_FILE_NAME,
        zip_type='zip'
    )


def _custom_download_url(
    url: str,
    folder: str
):
    filename = url.rpartition('/')[2].split('?')[0]
    path = osp.join(folder, filename)

    if osp.exists(path):  # pragma: no cover
        print('Using exist file', filename)
        return path

    print('Downloading', url)

    create_directory(folder)
    response = requests.get(url)
    with open(path, 'wb') as f:
        f.write(response.content)
    return path


def _download_and_unzip(
    file_url: str,
    dataset_dir: str,
    unzipped_file_name: str,
    zip_type: str
):
    """Downloads and unzips a file.

    Args:
        file_url (str): URL to the the file which should be downloaded.
        unzipped_file_name (str): Name under which output unzipped file
            should be saved.
        dataset_dir (str): Directory of the dataset.
        zip_type (str): Compression type.
    """
    raw_data_dir = osp.join(dataset_dir, 'raw_data')
    file_path = _custom_download_url(
        file_url,
        raw_data_dir
    )
    if zip_type == 'gz':
        with gzip.open(
            file_path,
            'rb'
        ) as f:
            file_content = f.read()
        output_file_path = osp.join(
            dataset_dir,
            'raw_data',
            unzipped_file_name
        )
        with open(
            output_file_path,
            'wb'
        ) as f:
            f.write(file_content)
    elif zip_type == 'zip':
        with zipfile.ZipFile(file_path, 'r') as zip_ref:
            zip_ref.extractall(raw_data_dir)
    else:
        raise NotSupportedCompression


def preprocess_miner_disease_chemical(data_path: str) -> Tuple[str, str]:
    """Processes and save raw DDI data from and to a given data_path.

    Processes the raw data into separate csvs with:
        - edge index
            (saved to: `data_path`/processed/disease_chemical_edge_index.csv)
        - node labels
            (saved to: `data_path`/processed/disease_chemical_nodes_labels.csv)

    Additionally it saves:
        - label encoder for nodes (original node id: encoded id)
            output is saved to: `data_path`/processed/label_encoder.pkl
        - dictionary with labels for nodes (original node id: node label id)
            output is saved to: `data_path`/processed/labels_dict.pkl

    Args:
        data_path (str): Path to the raw data for DDI.

    Returns:
        Tuple[str, str]: Tuple with directories to the output files with:
            - edge index - csv with two columns for source and target node
            - node labels - csv with two columns node and it's label
                (0: chemical, 1: disease)
    """
    raw_data_dir = osp.join(
        data_path,
        'raw_data',
        FILE_NAME
    )
    df = pd.read_csv(
        raw_data_dir,
        sep='\t'
    )
    df = _clean_raw_df(df)

    nodes = np.concatenate([df.iloc[:, 0].unique(), df.iloc[:, 1].unique()])
    le = LabelEncoder()
    nodes_encoded = le.fit_transform(nodes)

    storage_dir = osp.join(data_path, 'processed')
    df['source'] = le.transform(df.iloc[:, 0])
    df['target'] = le.transform(df.iloc[:, 1])

    edge_index_dir = osp.join(
        storage_dir,
        'disease_chemical_edge_index.csv'
    )
    nodes_labels_dir = osp.join(
        storage_dir,
        'disease_chemical_nodes_labels.csv'
    )

    df[['source', 'target']].to_csv(
        edge_index_dir,
        index=False
    )

    labels = np.zeros(nodes.shape[0])
    labels[df.iloc[:, 0].unique().shape[0]:] = 1
    labels_df = pd.DataFrame(
        zip(nodes_encoded, labels),
        columns=['node', 'label']
        )
    labels_df.to_csv(
        nodes_labels_dir,
        index=False
        )

    labels_dict = dict(zip(nodes, labels))
    save_to_pickle(
        labels_dict,
        f"{storage_dir}/labels_dict.pkl"
    )
    save_to_pickle(
        le,
        f"{storage_dir}/label_encoder.pkl"
    )

    return edge_index_dir, nodes_labels_dir


def _clean_raw_df(df: pd.DataFrame) -> pd.DataFrame:
    """Removes repeated node pairs.

    Args:
        df (pd.DataFrame): Pandas dataframe with raw data for DDI.

    Returns:
        pd.DataFrame: Cleaned dataframe with DDI data.
    """
    indices = df[df.iloc[:, 0] == df.columns[0]].index.values
    df = df.drop(indices)
    return df


def NotSupportedCompression(Exception):
    """Raised when provided compression type is not supportd"""
    pass
