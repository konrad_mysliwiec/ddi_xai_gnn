import os
import pickle


def create_directory(directory: str):
    if not os.path.exists(directory):
        os.makedirs(directory)


def save_to_pickle(obj, file_path: str):
    with open(file_path, 'wb') as f:
        pickle.dump(obj, f)


def load_from_pickle(file_path: str):
    with open(file_path, 'rb') as f:
        return pickle.load(f)
